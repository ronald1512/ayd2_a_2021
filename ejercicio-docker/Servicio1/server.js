const express =  require('express');

//Constants
const port = 3000;
const app = express();


app.get('/', (req, res)=>{
	res.send('Hello World!');
});

app.listen(port, ()=>{
	console.log('Example app listening at localhost:3000');
})

